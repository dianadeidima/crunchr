// Count `config.yml` files
export const countConfigYmlFiles = (list) => {
  const lines = list.split("\n")
  return lines.filter((line) => line.trim().toLowerCase().endsWith("config.yml")).length
}

// Count number of unique clients
export const countNumberOfClients = (list) => {
  const uniqueClients = new Set()

  list.split("\n").forEach((line) => {
    // Extract and normalize client name
    const clientName = line.trim().split("/")[0].toLowerCase().trim()

    // Add non-empty client names to the set
    if (clientName) {
      uniqueClients.add(clientName)
    }
  })

  uniqueClients.delete("") // Remove empty entry if any

  return uniqueClients.size
}

// Compare unique clients with `config.yml` files
export const compareUniqueClientsWithConfigYmlFiles = (list) => {
  const lines = list.split("\n")
  const uniqueClients = new Set()
  let configCount = 0

  lines.forEach((line) => {
    const file = line.trim()
    const clientName = file.split("/")[0].toLowerCase()
    uniqueClients.add(clientName)

    // Check if the line ends with "config.yml" and increment the counter
    if (file.toLowerCase().endsWith("config.yml")) {
      configCount++
    }
  })
  return { uniqueClientsSize: uniqueClients.size, configCount }
}

// Find missing `config.yml` files
export const findMissingConfigYmlFiles = (list) => {
  const lines = list.split("\n")
  const clientsWithConfig = new Set()
  const clientsInFile = new Set()

  lines.forEach((line) => {
    const file = line.trim()
    const clientName = file.split("/")[0].toLowerCase()

    // Skip empty lines
    if (file === "") {
      return
    }

    clientsInFile.add(clientName)

    if (file.toLowerCase().endsWith("config.yml")) {
      clientsWithConfig.add(clientName)
    }
  })

  // Find missing clients (in clientsInFile but not in clientsWithConfig)
  const missingClients = [...clientsInFile].filter((client) => !clientsWithConfig.has(client))
  return missingClients
}

// Find all most common timeslices
export const findAllMostCommonTimeslices = (list) => {
  const timesliceCount = {}

  list.split("\n").forEach((line) => {
    // Match timeslice pattern in the line
    const match = line.trim().match(/\/ts\/(\d{4}\/\d{1,2})\.csv$/)
    // If a match is found, update the count in timesliceCount
    if (match) {
      const timeslice = match[1]
      timesliceCount[timeslice] = (timesliceCount[timeslice] || 0) + 1
    }
  })
  const maxCount = Math.max(...Object.values(timesliceCount))
  // Find all timeslices with the maximum count
  return Object.keys(timesliceCount).filter((key) => timesliceCount[key] === maxCount)
}

// Forgotten upload alert
export const forgottenUploadAlert = (list, currentDate = new Date()) => {
  currentDate.setDate(1) // First day of the current month
  const inactiveClients = new Set()

  list.split("\n").forEach((line) => {
    // Trim any leading or trailing whitespaces from the line
    const match = line.trim().match(/\/ts\/(\d{4})\/(\d{1,2})\.csv$/)
    // Check if the line matches the expected timeslice file format
    if (match) {
      const [, year, month] = match
      const timesliceDate = new Date(`${year}-${month}-01`)
      // Calculate the difference in months between the current date and the timeslice date
      const monthsDifference =
        (currentDate.getFullYear() - timesliceDate.getFullYear()) * 12 +
        currentDate.getMonth() -
        timesliceDate.getMonth()

      // Check if the difference is greater than 3 months
      if (monthsDifference > 3) {
        const clientName = line.trim().split("/")[0]
        inactiveClients.add(clientName)
      }
    }
  })
  return Array.from(inactiveClients)
}
