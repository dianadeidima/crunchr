import {
  countConfigYmlFiles,
  countNumberOfClients,
  compareUniqueClientsWithConfigYmlFiles,
  findMissingConfigYmlFiles,
  findAllMostCommonTimeslices,
  forgottenUploadAlert,
} from "../support/file-reading-methods"

const listings = "cypress/full-upload-listing.txt"

describe("Crunchr Software Engineering Assessment", () => {
  let listContent

  before(() => {
    // Read the file once before all tests
    cy.readFile(listings).then((list) => {
      listContent = list
    })
  })

  it("Count config.yml files", () => {
    const configCount = countConfigYmlFiles(listContent)

    // Validate that the amount of config files is not 0
    expect(configCount).to.be.gte(1)
    cy.log(`Number of config.yml files: ${configCount}`)
  })

  it("Count number of clients", () => {
    const numberOfClients = countNumberOfClients(listContent)

    // Validate that the amount of config files is not 0
    expect(numberOfClients).to.be.greaterThan(0)
    cy.log(`Number of unique clients: ${numberOfClients}`)
  })

  it("Compare unique clients with config.yml files", () => {
    const { uniqueClientsSize, configCount } = compareUniqueClientsWithConfigYmlFiles(listContent)

    // Validate that the ampunt of config files equals the amount of clients
    expect(uniqueClientsSize).to.equal(configCount)
    cy.log(`Number of unique clients: ${uniqueClientsSize}`)
    cy.log(`Number of config.yml files: ${configCount}`)
  })

  it("Finding the missing config.yml files", () => {
    const missingClients = findMissingConfigYmlFiles(listContent)
    expect(missingClients.length).to.be.greaterThan(0)
    cy.log(`Missing config.yml files for clients: ${missingClients.join(", ")}`)
  })

  it("Find all most common timeslices", () => {
    const mostCommonTimeslices = findAllMostCommonTimeslices(listContent)
    expect(mostCommonTimeslices.length).to.be.greaterThan(0)
    cy.log(`Most common timeslices: ${mostCommonTimeslices.join(", ")}`)
  })

  it("Forgotten upload alert", () => {
    const inactiveClients = forgottenUploadAlert(listContent)
    if (inactiveClients.length > 0) {
      cy.log(`Clients who haven't uploaded for more than 3 months: ${inactiveClients.join(", ")}`)
    } else {
      cy.log("All clients are up to date.")
    }
  })
})
