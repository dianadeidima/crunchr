# Crunchr Software Engineering Assessment

This repository contains Cypress tests developed for the Crunchr Software Engineering Assessment. The tests are designed to validate various functionalities related to client data, configuration files, and upload alerts. The Author added an extra test that seemed logical for this specific funtionality and that showes that there is the data file isn't complete.

## Requirements

Before running the tests, ensure you have the following installed:

- [Node.js](https://nodejs.org/)

- [Cypress](https://www.cypress.io/)

## Setup

1. Clone this repository:

```bash
git clone https://gitlab.com/dianadeidima/crunchr.git
```

2. Navigate to the project directory:

```bash
cd crunchr
```

3. Install dependencies:

```bash
npm install
```

## Running Tests

To run the tests, execute the following command in the project directory:

```bash
npx cypress open
```

This will open the Cypress Test Runner, allowing you to run and interact with the tests.

All test are located in cypress/e2e/import-tests.cy.js

Personally, I would split tests according to their functionality and use dynamic data with customers' files. Depending on the tests and their complexity I prefer to store methods that can be reused outside of the tests, to maintain clean code and DRY principle. With this, methods can be accessed by any other project contributor.

## Test Descriptions

### Count config.yml files

This test counts the number of unique clients by extracting client names from file paths and ensuring the count is greater than zero.

### Count the number of clients

This test identifies clients with missing config.yml files by comparing the list of clients with actual config files and logging the missing clients.

### Compare unique clients with config.yml files

This test ensures that the amount of unique clients is the same as that of config.yml files.

### Find All Most Common Timeslices

This test determines the most common timeslice (year+month combo) between all clients by analyzing the file paths and logging the result.

### Forgotten Upload Alert

This test identifies clients who haven't uploaded data for more than 3 months, using the current date dynamically.

## Authors

- [@dianadeidima](https://gitlab.com/dianadeidima)

If you have any questions, feedback, or would like to discuss the tests further, I am more than happy to engage in open conversations during the interview :) Feel free to bring up any queries or points for discussion.
